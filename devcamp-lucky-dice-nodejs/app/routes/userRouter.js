// Import bộ thư viện express
const express = require('express'); 

//Import middleware
const luckydiceMiddleware = require('../middlewares/luckydiceMiddleware')

//Tạo 1 instance router
const router = express.Router();

//Import controller
const {
  createUser,
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById
} = require('../controllers/userController');

router.use('/', luckydiceMiddleware);

router.post('/users', createUser);

router.get('/users', getAllUser);

router.get('/users/:userId', getUserById);

router.put('/users/:userId', updateUserById);

router.delete('/users/:userId', deleteUserById);

module.exports = router;